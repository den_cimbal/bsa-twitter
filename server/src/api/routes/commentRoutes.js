import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && (reaction.comment.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked or disliked his comment
        if (reaction.dataValues.isLike) {
          req.io.to(reaction.comment.userId).emit('comment_like', 'Your comment was liked!');
        } else {
          req.io.to(reaction.comment.userId).emit('comment_dislike', 'Your comment was disliked!');
        }
      }
      return res.send(reaction);
    })
    .catch(next));

export default router;
